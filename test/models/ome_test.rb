require "test_helper"

class OmeTest < ActiveSupport::TestCase
  test "should not save ome without name" do
    # assert true
    ome = Ome.new
    assert_not ome.save, "Saved the article without a title"
  end
  test "should not save ome without description" do
    # assert true
    ome = Ome.new({name: "name", image:""})
    assert_not_nil( ome.image, "image is nil" )
    assert_not ome.save, "Saved the article without a title"
    assert_equal  "name", ome.name, "name value is not equal"
  end
end
